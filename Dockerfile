FROM node:16.17-slim

WORKDIR /usr/src/app

# Env variables
ARG MAP_KEY
ARG WEATHER_KEY
ENV MAP_KEY=$MAP_KEY
ENV WEATHER_KEY=$WEATHER_KEY

COPY . ./
RUN yarn install --silent
RUN yarn build --silent

CMD ["node", "server.js"]
