import faker from 'faker';

import project1 from '../assets/img/projects/project1.jpg';
import project2 from '../assets/img/projects/project2.jpg';
import project3 from '../assets/img/projects/project3.jpg';
import project4 from '../assets/img/projects/project4.jpg';;

const works = [
  {
    name: faker.lorem.sentence(),
    customer: "G&L, Inc.",
    description: faker.lorem.paragraphs(),
    image: project1
  },
  {
    name: faker.lorem.sentence(),
    customer: "Smith Ltd.",
    description: faker.lorem.paragraphs(),
    image: project2
  },
  {
    name: faker.lorem.sentence(),
    customer: "Oberland Industries",
    description: faker.lorem.paragraphs(),
    image: project3
  },
  {
    name: faker.lorem.sentence(),
    customer: "FHBC",
    description: faker.lorem.paragraphs(),
    image: project4
  }
];

export default works;
