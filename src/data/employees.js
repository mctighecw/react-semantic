import faker from 'faker';

import person1 from '../assets/img/team/elyse.png';
import person2 from '../assets/img/team/kristy.png';
import person3 from '../assets/img/team/matthew.png';
import person4 from '../assets/img/team/molly.png';
import person5 from '../assets/img/team/elliot.jpg';
import person6 from '../assets/img/team/sam.jpg';

const employees = [
  {
    name: "Elyse",
    role: "Accounting",
    description: faker.lorem.sentence(),
    image: person1
  },
  {
    name: "Kristy",
    role: "Customer Support",
    description: faker.lorem.sentence(),
    image: person2
  },
  {
    name: "Matthew",
    role: "Sales",
    description: faker.lorem.sentence(),
    image: person3
  },
  {
    name: "Molly",
    role: "HR",
    description: faker.lorem.sentence(),
    image: person4
  },
  {
    name: "Elliot",
    role: "CEO",
    description: faker.lorem.sentence(),
    image: person5
  },
  {
    name: "Sam",
    role: "Graphic Design",
    description: faker.lorem.sentence(),
    image: person6
  }
];

export default employees;
