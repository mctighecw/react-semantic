import faker from 'faker';
import _ from 'lodash';

const sentences = _.times(6, () => ({
  text: faker.lorem.sentence() + " " + faker.lorem.sentence() + ".."
}));

const articles = [
  {
    title: faker.lorem.sentence(),
    author: "John Smith",
    date: "2 September 2017",
    rating: 3,
    text: sentences[0].text
  },
  {
    title: faker.lorem.sentence(),
    author: "Maria Schmidt",
    date: "29 August 2017",
    rating: 4,
    text: sentences[1].text
  },
  {
    title: faker.lorem.sentence(),
    author: "Robert Miller",
    date: "25 August 2017",
    rating: 2,
    text: sentences[2].text
  },
  {
    title: faker.lorem.sentence(),
    author: "Max Obermann",
    date: "22 August 2017",
    rating: 4,
    text: sentences[3].text
  },
  {
    title: faker.lorem.sentence(),
    author: "Jean Le Blanc",
    date: "20 August 2017",
    rating: 3,
    text: sentences[4].text
  },
  {
    title: faker.lorem.sentence(),
    author: "Susan Williamson",
    date: "17 August 2017",
    rating: 5,
    text: sentences[5].text
  }
];

export default articles;
