import faker from 'faker';

const jobs = [
  {
    title: "HR",
    time: "Full time",
    description: faker.lorem.sentence()
  },
  {
    title: "Sales",
    time: "Full time",
    description: faker.lorem.sentence()
  },
  {
    title: "IT",
    time: "Part time",
    description: faker.lorem.sentence()
  },
  {
    title: "Accounting",
    time: "Part time",
    description: faker.lorem.sentence()
  },
  {
    title: "Secretary",
    time: "Full time",
    description: faker.lorem.sentence()
  },
  {
    title: "R&D",
    time: "Part time",
    description: faker.lorem.sentence()
  }
];

export default jobs;
