import React, { Component } from 'react';
import SocialMedia from './elements/SocialMedia';

class Footer extends Component {
  render() {
    return (
      <div className="footer-content">
        <SocialMedia />
        <div className="copyright">
          &copy; 2017, Christian McTighe. Coded by Hand.
        </div>
      </div>
    );
  }
}

export default Footer;
