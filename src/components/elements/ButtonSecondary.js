import React from 'react';
import { Button } from 'semantic-ui-react';

const ButtonSecondary = () => (
  <Button secondary size="medium">Secondary</Button>
)

export default ButtonSecondary;
