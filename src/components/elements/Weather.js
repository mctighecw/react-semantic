import React, { Component } from 'react';
import { Segment, Grid } from 'semantic-ui-react';
import axios from 'axios';
import 'open-weather-icons/scss/open-weather-icons.scss';

const WEATHER_KEY = process.env.WEATHER_KEY;

class Weather extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weatherData: null
    };
  }

  componentWillMount() {
    const self = this;
    const url = 'https://api.openweathermap.org/data/2.5/weather?id=5128581&APPID=' + WEATHER_KEY + '&units=metric';

    axios({
      method:'get',
      url: url
    })
    .then(function(response) {
      //console.log(response);
      const weatherData = response.data;

      self.setState({ weatherData });
    })
    .catch(function (error) {
      console.log(error);
    });
  }

   render() {
    const { weatherData } = this.state;

    return (
      <div className="weather">
        <Segment>
          <h3>Current Weather</h3>
            {weatherData !== null ? (
              <Grid>
              <Grid.Row columns={2}>
                <Grid.Column width={12}>
                  <h4>Description: {weatherData.weather[0].main}<br />
                  Temperature: {Math.round(weatherData.main.temp)} °C<br />
                  Max Temp.: {weatherData.main.temp_max} °C</h4>
                </Grid.Column>
                <Grid.Column width={4}>
                  <i className={"owi owi-" + weatherData.weather[0].icon}></i>
                </Grid.Column>
              </Grid.Row>
              </Grid>
            ) : <h4>No weather data available.</h4> }
        </Segment>
      </div>
    )
  }
}

export default Weather;
