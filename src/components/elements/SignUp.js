import React, { Component } from 'react'
import { Form, Input, Button, Modal, Message } from 'semantic-ui-react';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      formSubmitted: false
    }
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleCancel = () => {
    this.props.handleCloseModal();
    this.setState({ formSubmitted: false });
    this.clearState();
  }

  handleSubmit = () => {
    let { name, email } = this.state;

    // POST info to backend
    this.setState({ formSubmitted: true });
    this.clearState();
  }

  clearState = () => {
    this.setState({
      name: '',
      email: ''
    });
  }

  render() {
    let submitDisabled = this.state.name.length < 3 || this.state.email === '' ? true : false;

    return (
      <Modal open={this.props.modalOpen}>
        <Modal.Header>Newsletter</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {this.state.formSubmitted === false ?  (
                <div>
                  <p><i>To sign up for our newsletter, please enter your name and email address.</i></p>
                  <Form onSubmit={this.handleSubmit}>
                    <Form.Group widths='equal'>
                      <Form.Field>
                        <Input required type="text" placeholder='Name' name='name' onChange={this.handleChange} />
                      </Form.Field>
                      <Form.Field>
                        <Input required type="email" placeholder='Email' name='email' onChange={this.handleChange} />
                      </Form.Field>
                    </Form.Group>
                    <Form.Group>
                      <Form.Button secondary content='Cancel' type="button" onClick={this.handleCancel} />
                      <Form.Button primary content='Submit' disabled={submitDisabled} />
                    </Form.Group>
                  </Form>
                </div>
              ) : (
                <div>
                  <Message
                    success
                    header='Submitted'
                    content="You're all signed up for the newsletter."
                  />
                  <Form.Button primary content='Continue' type="button" onClick={this.handleCancel} />
                </div>
            )}
          </Modal.Description>
        </Modal.Content>
      </Modal>
    )
  }
}

export default SignUp;
