import React from 'react';
import { Button } from 'semantic-ui-react';

const ButtonPrimary = () => (
  <Button primary size="medium">Primary</Button>
)

export default ButtonPrimary;
