import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';

class SocialMedia extends Component {

  onClick = (sm) => alert("Going to " + sm);

   render() {
    return (
      <div>
        <Button circular color='facebook' icon='facebook' onClick={() => { this.onClick('Facebook'); }} />
        <Button circular color='twitter' icon='twitter' onClick={() => { this.onClick('Twitter'); }} />
        <Button circular color='linkedin' icon='linkedin' onClick={() => { this.onClick('LinkedIn'); }} />
        <Button circular color='google plus' icon='google plus' onClick={() => { this.onClick('Google Plus'); }} />
      </div>
    )
  }
}

export default SocialMedia;
