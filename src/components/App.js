import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Segment, Menu, Icon, Sidebar, Image } from 'semantic-ui-react';
import '../styles/styles.less';

import logo from '../assets/img/logo.png';
import Main from '../Main';
import Footer from './Footer';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuVisible: false
    }
  }

  render() {
    return (
      <div>
        <Menu secondary attached="top">
          <Menu.Item onClick={() => this.setState({ menuVisible: !this.state.menuVisible })} >
          <Icon
            name="sidebar"
            size="small"
          />
          </Menu.Item>
          <Menu.Menu position='right'>
            <Menu.Item>
              <Image
                src={logo}
                size="small"
                alt="logo"
              />
            </Menu.Item>
          </Menu.Menu>
        </Menu>

        <Sidebar.Pushable as={Segment} attached="bottom" >
          <Sidebar as={Menu} animation="uncover" visible={this.state.menuVisible} icon="labeled" width='thin' vertical inverted>
            <Menu.Item as={Link} to='/'><Icon name="home" />Home</Menu.Item>
            <Menu.Item as={Link} to='/about'><Icon name="info" />About</Menu.Item>
            <Menu.Item as={Link} to='/projects'><Icon name="sitemap" />Projects</Menu.Item>
            <Menu.Item as={Link} to='/team'><Icon name="user circle outline" />Team</Menu.Item>
            <Menu.Item as={Link} to='/jobs'><Icon name="suitcase" />Jobs</Menu.Item>
            <Menu.Item as={Link} to='/faq'><Icon name="question" />FAQ</Menu.Item>
            <Menu.Item as={Link} to='/contact'><Icon name="mail" />Contact</Menu.Item>
            <Menu.Item as={Link} to='/blog'><Icon name="write" />Blog</Menu.Item>
          </Sidebar>

          <Sidebar.Pusher>
            <div className="page">
              <div className="main">
                <Main />
              </div>
            </div>

            <div className="footer">
              <Footer />
            </div>

          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>
    );
  }
}

export default App;
