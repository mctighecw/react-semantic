import React, { Component } from 'react';
import { Container, Segment, Accordion } from 'semantic-ui-react';
import faker from 'faker';
import _ from 'lodash';

const panels = _.times(10, () => ({
  title: faker.lorem.sentence(),
  content: faker.lorem.paragraphs(),
}));

class Faq extends Component {
  render() {
    return (
      <div>
        <div className="faq">
          <Container text>
            <Segment.Group>
              <Segment>
                <h1>FAQ</h1>
                <Segment>
                  <Accordion panels={panels} />
                </Segment>
              </Segment>
            </Segment.Group>
          </Container>
        </div>
      </div>
    );
  }
}

export default Faq;
