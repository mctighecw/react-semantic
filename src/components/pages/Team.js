import React, { Component } from 'react';
import { Container, Segment, Grid, Card, Image } from 'semantic-ui-react';

import users from '../../data/employees';

class Team extends Component {
  render() {
    return (
      <div>
        <div className="team">
          <Container text>
            <Segment.Group>
              <Segment>
              <h1>Our Team</h1>
              <h3>Coming from various countries and diverse backgrounds, we make up a dynamic, interesting team.</h3>

              <Grid>
                {users ? users.map(function(user, index) {
                  return (
                    <Grid.Column mobile={16} tablet={8} computer={8} key={index}>
                      <Card>
                        <Image src={user.image} className="employee-pic" />
                        <Card.Content>
                          <Card.Header>{user.name}</Card.Header>
                          <Card.Meta>{user.role}</Card.Meta>
                          <Card.Description>{user.description}</Card.Description>
                        </Card.Content>
                      </Card>
                    </Grid.Column>
                    )
                  })
                : null }
              </Grid>

              </Segment>
            </Segment.Group>
          </Container>
        </div>
      </div>
    );
  }
}

export default Team;
