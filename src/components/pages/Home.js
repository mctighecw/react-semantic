import React, { Component } from 'react';
import { Container, Segment, Header, Message } from 'semantic-ui-react';
import faker from 'faker';

import office from '../../assets/img/office.jpg';
import SignUp from '../elements/SignUp';

class Home extends Component {
  state = {
    modalOpen: false
  }

  handleOpenModal = () => this.setState({ modalOpen: true });

  handleCloseModal = () => this.setState({ modalOpen: false });

  render() {
    return (
      <div>
        <div className="home">
          <Segment>
            <h1>Welcome to Lorem Ipsum</h1>

            <img
              src={office}
              alt="office"
              className="office"
            />
            <br />

            <Container className="home-box">
              <Segment>
                <h2>A Modern Company</h2>
                <p>{faker.lorem.paragraphs()}</p>
                <p>{faker.lorem.paragraphs()}</p>
                <p>{faker.lorem.paragraphs()}</p>
              </Segment>

              <div className="mailing-list-box">
                  <Message
                    icon='inbox'
                    header='Join our mailing list'
                    content='Sign up and get periodic news updates from us.'
                    onClick={this.handleOpenModal}
                  />
                </div>
              </Container>
            </Segment>
          <SignUp modalOpen={this.state.modalOpen} handleCloseModal={this.handleCloseModal} />

        </div>
      </div>
    );
  }
}

export default Home;
