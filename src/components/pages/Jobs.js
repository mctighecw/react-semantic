import React, { Component } from 'react';
import { Container, Segment, Grid, Label } from 'semantic-ui-react';

import jobs from '../../data/jobs';

class Jobs extends Component {
  render() {
    return (
      <div>
        <div className="jobs">
          <Container text>
            <Segment.Group>
              <Segment>
                <h1>Job Openings</h1>
                <h3>Currently, we have the following openings.</h3>
                <Label as='a' color='red' ribbon>Apply now!</Label>

                <Grid container stackable>
                  <Grid.Row columns={2}>
                    {jobs ? jobs.map(function(job, index) {
                      return (
                        <Grid.Column className="job-box" key={index}>
                          <Segment>
                            <h4><b>{job.title}</b><br />
                            <span className="job-time">{job.time}</span></h4>
                            <p>{job.description}</p>
                          </Segment>
                        </Grid.Column>
                        )
                      })
                    : null }
                  </Grid.Row>

                </Grid>

              </Segment>
            </Segment.Group>
          </Container>
        </div>
      </div>
    );
  }
}

export default Jobs;
