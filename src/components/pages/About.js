import React, { Component } from 'react';
import { Container, Segment, Grid, Tab, Responsive } from 'semantic-ui-react';
import faker from 'faker';

class About extends Component {
  render() {
    const aboutUs = (
      <Segment>
        <h1>About Us</h1>
        <Segment>
          <p>{faker.lorem.paragraphs()}</p>
          <p>{faker.lorem.paragraphs()}</p>
          <p>{faker.lorem.paragraphs()}</p>
        </Segment>
      </Segment>
    );

    const areasOfWork = (
      <Segment>
        <h1>Areas of Work</h1>
          <Grid celled container stackable>
            <Grid.Row columns={2}>
              <Grid.Column>
                <Segment>Education</Segment>
              </Grid.Column>
              <Grid.Column>
                <Segment>Research</Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={3}>
              <Grid.Column>
                <Segment>Technology</Segment>
              </Grid.Column>
              <Grid.Column>
                <Segment>Finance</Segment>
              </Grid.Column>
              <Grid.Column>
                <Segment>Consulting</Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2}>
              <Grid.Column>
                <Segment>Medicine</Segment>
              </Grid.Column>
              <Grid.Column>
                <Segment>Legal</Segment>
              </Grid.Column>
            </Grid.Row>
          </Grid>
      </Segment>
    );

    const futurePlans = (
      <Segment>
        <h1>Future Research</h1>
        <Segment>
          <p>{faker.lorem.paragraphs()}</p>
        </Segment>
      </Segment>
    );

    const panes = [
      { menuItem: 'About Us', render: () => <Tab.Pane>{aboutUs}</Tab.Pane> },
      { menuItem: 'Areas of Work', render: () => <Tab.Pane>{areasOfWork}</Tab.Pane> },
      { menuItem: 'Future Plans', render: () => <Tab.Pane>{futurePlans}</Tab.Pane> }
    ]

    return (
      <div>
        <div className="about">
          <Container>
            <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
          </Container>
        </div>
      </div>
    );
  }
}

export default About;
