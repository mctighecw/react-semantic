import React, { Component } from 'react';
import { Container, Grid, Segment, Image } from 'semantic-ui-react';

import works from '../../data/works';

class Projects extends Component {
  render() {
    return (
      <div>
        <div className="projects">
          <Container text>
            <Segment.Group>
              <Segment>
                <h1>Projects</h1>
                <h3>Here is a sample of some of our current projects.</h3>

                <Grid>
                  {works ? works.map(function(work, index) {
                    return (
                      <Grid.Column mobile={16} tablet={8} computer={8} key={index}>
                        <Segment>
                          <Image src={work.image} className="project-pic" />
                          <h4><b>{work.name}</b><br />
                          <i>{work.customer}</i></h4>
                          <p>{work.description}</p>
                        </Segment>
                      </Grid.Column>
                      )
                    })
                  : null }
                </Grid>

              </Segment>
            </Segment.Group>
          </Container>
        </div>
      </div>
    );
  }
}

export default Projects;
