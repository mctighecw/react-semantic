import React, { Component } from 'react';
import { Container, Segment, Grid } from 'semantic-ui-react';
import iFrame from 'react-iframe';
import Weather from '../elements/Weather';

const MAP_KEY = process.env.MAP_KEY;

class Contact extends Component {
  render() {
    const url = "https://www.google.com/maps/embed/v1/place?&q=100+W+23rd+St,+New+York,+NY+10011,+USA&zoom=15&key=" + MAP_KEY;

    return (
      <div>
        <div className="contact">
          <Container text>
            <Segment.Group>
              <Segment>
                <h1>Contact</h1>
                <h3>Please feel free to get in touch with us. We would love to hear from you.</h3>

                <Grid container stackable style={{paddingBottom: 10}}>
                  <Grid.Row columns={2}>
                    <Grid.Column width={8}>
                      <div className="contact-info">
                        <h3>Lorem Ipsum, Inc.<br />
                        100 W 23rd St<br />
                        New York, NY 10011, USA<br />
                        <a href="">info@lorem-ipsum.com</a></h3>
                      </div>
                    </Grid.Column>
                    <Grid.Column width={8} style={{paddingRight: 0}}>
                      <Weather />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>

                <div>
                  <Segment>
                    <div className="iframe-loading">
                      <iFrame
                        width="100%"
                        height="450px"
                        style={{border: 0}}
                        src={url}
                        allowFullScreen />
                      </div>
                  </Segment>
                </div>

              </Segment>
            </Segment.Group>
          </Container>
        </div>
      </div>
    );
  }
}

export default Contact;
