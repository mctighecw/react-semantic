import React, { Component } from 'react';
import { Container, Segment, Divider, Label, Rating } from 'semantic-ui-react';

import articles from '../../data/articles';

class Blog extends Component {
  render() {
    return (
      <div>
        <div className="blog">
          <Container text>
            <Segment.Group>
              <Segment>
                <h1>Blog</h1>
                <Label as='a' color='blue' ribbon>Follow us!</Label>

                <Divider />
                {articles ? articles.map(function(article, index) {
                  return (
                    <div key={index}>
                      <h3><b>{article.title}</b><br />
                      by {article.author}</h3>
                      <div><span><i>{article.date}</i></span><br />
                      <Rating icon='star' rating={article.rating} maxRating={5} /></div><br />
                      <p>{article.text}</p>
                      {index < articles.length -1 ? <Divider /> : null}
                    </div>
                    )
                  })
                : <h3>Currently, there are no blog entries. Please check back later.</h3> }
              </Segment>
            </Segment.Group>
          </Container>
        </div>
      </div>
    );
  }
}

export default Blog;
