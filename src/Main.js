import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './components/pages/Home';
import About from './components/pages/About';
import Projects from './components/pages/Projects';
import Team from './components/pages/Team';
import Jobs from './components/pages/Jobs';
import Faq from './components/pages/Faq';
import Contact from './components/pages/Contact';
import Blog from './components/pages/Blog';

const Main = () => (
  <div>
    <Switch>
      <Route exact path='/' component={Home} />
      <Route path='/about' component={About} />
      <Route path='/projects' component={Projects} />
      <Route path='/team' component={Team} />
      <Route path='/jobs' component={Jobs} />
      <Route path='/faq' component={Faq} />
      <Route path='/contact' component={Contact} />
      <Route path='/blog' component={Blog} />
      <Route path='*' component={Home} />
    </Switch>
  </div>
);

export default Main;
