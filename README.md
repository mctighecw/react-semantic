# README

This is a small React app to try out Semantic UI.

## App Information

App Name: react-semantic

Created: September 2017

Updated: Fall 2018; September 2022

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/react-semantic)

Production: [Link](https://react-semantic.mctighecw.site)

## Run with Docker

See `.env` file for real build-arg values.

```
$ docker build \
  --build-arg MAP_KEY=MAP_KEY_VALUE \
  --build-arg WEATHER_KEY=WEATHER_KEY_VALUE \
  -t react-semantic .

$ docker run --rm -it -p 80:5000 react-semantic
```

## Notes

- React
- React Router
- Semantic UI
- LESS
- Google Maps API
- Open Weather API & Icons
- Originally deployed on Heroku
- Moved to personal server in September 2022

Any feedback or comments would be appreciated.

Last updated: 2025-02-23
